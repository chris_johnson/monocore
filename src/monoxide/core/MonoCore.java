package monoxide.core;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.relauncher.IFMLCallHook;
import cpw.mods.fml.relauncher.IFMLLoadingPlugin;
import monoxide.core.libraries.LibraryManager;
import monoxide.core.logging.MonoLog;
import net.minecraft.launchwrapper.LaunchClassLoader;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.logging.Level;

@SuppressWarnings("JavacQuirks")
@IFMLLoadingPlugin.TransformerExclusions({
		"monoxide.core",
		"monoxide.core.asm",
		"monoxide.core.libraries",
		"monoxide.core.logging",
})
@Mod(modid = "MonoCore", name = "Monoxide's Mods", useMetadata = true)
public class MonoCore implements IFMLLoadingPlugin, IFMLCallHook {
	@SuppressWarnings("FieldCanBeLocal")
	private static final boolean runtimeDeobfuscationEnabled = true;
	private static final String deobfuscationFileName = null;
	private static final File mcLocation = null;

	private static final MonoLog logger = new MonoLog("MonoCore");

	@Override
	public String[] getLibraryRequestClass() {
		return new String[]{};
	}

	@Override
	public String[] getASMTransformerClass() {
		return new String[]{};
	}

	@Override
	public String getModContainerClass() {
		return null;
	}

	@Override
	public String getSetupClass() {
		return this.getClass().getCanonicalName();
	}

	@Override
	public void injectData(Map<String, Object> data) {
		for (String key : data.keySet()) {
			try {
				Field f = this.getClass().getDeclaredField(key);
				f.setAccessible(true);
				f.set(null, data.get(key));
			} catch (NoSuchFieldException e) {
				// Ignore this field if it doesn't exist
			} catch (IllegalAccessException e) {
				MonoCore.getLogger().log(Level.WARNING, e, "Unable to set field: %s", key);
			}
		}
	}

	@Override
	public Void call() throws Exception {
		LibraryManager.injectLibraries((LaunchClassLoader) Thread.currentThread().getContextClassLoader());

		return null;
	}

	public static MonoLog getLogger() {
		return logger;
	}

	public static File getMcLocation() {
		return mcLocation;
	}

	public static boolean getRuntimeDeobfuscationEnabled() {
		return runtimeDeobfuscationEnabled;
	}

	public static String getDeobfuscationFileName() {
		return deobfuscationFileName;
	}
}
