# MonoCore: #

This is a library of code used by all my mods. This is open source to facilitate
development on [ForgeBackup][forgebackup].

## Bug Reports: ##

Always happy to receive them, but I can't help you if you don't help me. If you
can't provide a log, then I probably can't help you. Please provide the console
output as well as the ForgeModLoader.log (typically either ForgeModLoader-client-0.log
or ForgeModLoader-server-0.log) from your minecraft folder. Please
[post issues on Bitbucket][bb-issues] if you are able. I'm also usually available on
[EsperNet IRC][irc] as monoxide.

## Permissions: ##

[MonoCore is an open source mod][bb]. It is released under [the MIT license][license].

  [bb]: https://bitbucket.org/monoxide0184/monocore
  [forgebackup]: https://bitbucket.org/monoxide0184/forgebackup
  [bb-issues]: https://bitbucket.org/monoxide0184/monocore/issues
  [license]: https://bitbucket.org/monoxide0184/monocore/src/master/LICENSE.md?at=master
  [irc]: http://webchat.esper.net/?channels=#monoxide

